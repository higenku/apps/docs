# Higenku Documentation (XDocs) [![pipeline status](https://gitlab.com/higenku/apps/docs/badges/dev/pipeline.svg)](https://gitlab.com/higenku/apps/docs/-/commits/dev)

This repository contains all documentation on Higenku Apps, Sdks and Tools, feel free to look around and mess with the stuff in here.

## How to run
```bash
pnpm # to install dependencies
pnpm dev # to run the project
pnpm build # to build the project to html files
```

## License
this project is license under the `GNU AFFERO GENERAL PUBLIC LICENSE, Version 3` as staided in [LICENSE](./LICENSE)

## Code of Conduct
we follow the [Higenku Code of Conduct](https://higenku.org/legal/CodeOfConduct/)

## Contributing
Please read our [Contributing Guide](/Contributing.md)