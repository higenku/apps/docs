# Higenku Apps
Higenku has various apps that are responsible for different things for different use cases. One of them is [XAccount](https://user.higenku.org), which is used to manage your account at the Higenku project, [XStore](https://store.higenku.org) which is responsible for distributing apps across the world, [XSuite](https://suite.higenku.org) which is our office tools for individuals and business and XClient which is responsible for interacting with XMan that handles installation, update, backup and management of apps inside your devices (across devices too).

### Users
With a Higenku Account ([XAccount](https://user.higenku.org)) you have access to a very large set of usable things for apps to implement. For example, all sensitive information in our servers are encrypted with military grade encryption (for the ones interested we use RSA and AES for that) and some of that information is end-to-end encrypted (which means that instead of encrypting the data on the server and then saving it to the database, the data already leaves your device encrypted)

With XStore authorized to access your XAccount, you will be allowed to access our Sync capabilities for all the apps you install in your system with XClient, purchase apps and install them in your system, and (if you are on Linux for example) sync the Desktop Environment settings.

### Developers

Furthermore, we offer APIs for developers to interact with our ecosystem. If, for example, you (as a developer) want to publish a paid application inside our store. You wouldn't have to worry about payment handling, creating and handling customer sensitive information (like credit cards) and processing the payment with [Stripe](https://stripe.com) or something similar.

Instead, you would just mark your app as a paid app, select your payment type (subscription or one time purchase) and verify (via our api) that the user has access to that app. We will handle customer sensitive information, accounts, etc. You just need to worry about making the best app ever.