---
title: Introduction
description: Docs intro
layout: ~/layouts/MainLayout.astro
---


# Higenku Tools (Tools)
These are the tooling that we provide to help our community develop cross-platform software in a better, easier and simpler way. These tools are available for various purposes including packing, installing and verifying applications, backup the environment, manage installed applications, build applications from bynaries to help them run in our runtime

## Applications

<div class="table">

|Name    | Purpose                  | Link              |
| ---    | ---                      | ---               |
| Packer | Cross platform App Packer| [Here](./packer/) |
| Packer | Cross platform App Packer| [Here](./packer/) |
| Packer | Cross platform App Packer| [Here](./packer/) |
| Packer | Cross platform App Packer| [Here](./packer/) |

</div>