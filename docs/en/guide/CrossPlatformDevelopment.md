# Cross platform dev with XPlatform

## Introduction
If you have to compile and package you app for various platforms, have to make tons of configuration and this all needs a lot of time every time you need to package a new app, our platform is for you. Actually this is a good platform for everybody that is developing applications to various platforms, because we take advantage of the way linux handle applications and libraries while also providing our own implementation and libraries for cross platform development.

Some applications would need to be adapted to run on our system, others just need a little bit of configuration, which can be either written by hand or partly generated by our [XPacker](/en/tools/packer/). 

Let's take firefox as an example, we already have adapted it to run in our runtime inside linux, macos and windows, you can see it in [here](https://gitlab/higenku/extapps/org.mozilla.firefox). We needed to study how firefox worked under the hood to work on all of the different platforms, and took the archlinux version as an example to see what they did different. Then with the information we needed, we just wrote the CI script that will be running every week fetching for updates and bringing these updates to the repository where we can take then and distribute it to our users.

## PWA?
If you are writing a new application from scratch to test our platform, before you start consider the tecnologies and frameworks you are using. Apps like discord don't need acess to the filesystem, they just need local storage and session storage after all they run in the browser, for that purpose we develop a Application Runtime that was named after the technology XPwaR or in english Higenku Progressive Web Applications Runtime. In this runtime, we combine the power of the web with the fast and reliable power of rust through the [XKrome library](/en/sdks/krome/) finally [learn more](ways/pwa)

## [Java|Type]script

## C#

## Rust

## C, C++ or build system 

## Python

## Java

## Custom