<!-- declare home -->

# Higenku Software Development Kits (Sdks)
These are the libraries that we develop to help our community develop cross-platform software in a better, easier and simpler way. Most of those are Shared Libraries that you can link. We provide packs, which you can specify in **package.yml**, to facilitate the packing process of you application.

## Libraries
|Name    | Purpose                  | Link          |
| ---    | ---                      | ---           |
| XKrome | Cross platform Webview   | [Here](./krome/) |
| XThm   | Css Theme                | [Here](./theme/) |

