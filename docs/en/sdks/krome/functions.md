# XKrome Functions and Types

## Types

### Ptr
This is the **Ptr** definition that we use across our entire library, It defines a Ptr to a Window, Webview, Webcontext, EventLoop and so own
```cpp
typedef const uintptr_t *Ptr;
```

### Response
The Response Object is a way to comunicate between rust and other languages in a HTTP like way, when handling a request with the custom protocol method in your webview, you must pass a function that will return a response.
The response contains the content with is a byte[] containing the file, image, html or text (for example), a content_len whioch is the length of the content you are returning and the indentifier which is [mimetype](https://www.iana.org/assignments/media-types/media-types.xhtml)
```cpp
struct Response {
  uint8_t *content;
  uint32_t content_len;
  const char *indentifier;
};
```

### Position
It's a representation of 2d space with the x and y coordinates. we use it to determinate the position of windows and other stuff.
```cpp
struct Position {
  int32_t x;
  int32_t y;
};
```

### Size
It's the representation of width and height. We use it to determinate the size of Windows and Webviews for example.
```cpp
struct Size {
  uint32_t width;
  uint32_t height;
};
```

### CEventData
It's the representation of the data from an event. It's used to communicate between rust and c via [ffi](https://doc.rust-lang.org/nomicon/ffi.html)
```cpp
struct CEventData {
  const char *_type;
  const char *_subtype;
  const char *_data;
  const char *_result;
};
```

## Functions And Callbacks

### WebContext
```cpp
Ptr xk_webcontext_new(const char *data_dir);
```
This creates a new webcontext, which can be used to specify a new data directory. It can be used across webviews


```cpp
const char *xk_webcontext_data_directory(const Ptr *webcontext);
```
This gets the data directory of the given webcontext


```cpp
void xk_webcontext_set_allows_automation();
```
This sets the **allow_automation** property to true

### Webview
```cpp
Ptr xk_webview_builder_new();
```
This Creates a builder for a webview



```cpp
void xk_webview_builder_with_transparent(Ptr *builder, bool transparent);
```
this sets the transparency of the webview. Set it to true and you will have a transparent webview


```cpp
void xk_webview_builder_with_visible(Ptr *builder, bool visible);
```
This sets wheather the webview is visible


```cpp
void xk_webview_builder_with_initialization_script(Ptr *builder, const char *js);
```
This sets the first script to be executed when the webview is running


```cpp
void xk_webview_builder_with_custom_protocol(Ptr *builder, const char *name, Response (*req_handler (uint32_t, const char*));
```
This sets the handler of a custom protocol to a webview, you can use your own like **imy-custom.protocol://**


```cpp
void xk_webview_builder_with_url(Ptr *builder, const char *url);
```
Sets the Url of the webview


```cpp
Ptr xk_webview_builder_build(Ptr builder, Ptr win, const Ptr *web_context);
```
Builds the webview with a window and a webcontext, the builder and window will be free after this function have run.

### Window
```cpp
Ptr xk_window_new(const Ptr *eloop);
```
Creates a new window


```cpp
void xk_window_request_redraw(const Ptr *win);
```
Request a new draw for the window


```cpp
Position xk_window_inner_position(const Ptr *win);
```
Gets the inner position of the window


```cpp
Position xk_window_outer_position(const Ptr *win);
```
Gets the outer position of the window


```cpp
void xk_window_set_outer_position(const Ptr *win, int32_t x, int32_t y);
```
Sets the outer position of the window


```cpp
Size xk_window_inner_size(const Ptr *win);
```
Gets the inner size of the window


```cpp
void xk_window_set_inner_size(const Ptr *win, uint32_t width, uint32_t height);
```
Gets the inner size of the window


```cpp
Size xk_window_outer_size(const Ptr *win);
```
Gets the outer size of the window


```cpp
void xk_window_set_min_inner_size(const Ptr *win, int32_t width, int32_t height);
```
Sets the minimal inner size of the window


```cpp
void xk_window_set_max_inner_size(const Ptr *win, int32_t width, int32_t height);
```
Sets the maximal inner size of the window


```cpp
void xk_window_set_title(const Ptr *win, const char *title);
```
Sets the title of the window


```cpp
void xk_window_set_visible(const Ptr *win, bool visible);
```
Sets the visibility of the window


```cpp
void xk_window_set_focus(const Ptr *win);
```
Sets focus to the window


```cpp
void xk_window_set_resizable(const Ptr *win, bool resizable);
```
Sets weather the window is resizable


```cpp
void xk_window_set_minimized(const Ptr *win, bool minimized);
```
Sets weather the window is minimized


```cpp
void xk_window_set_maximized(const Ptr *win, bool maximized);
```
Sets weather the window is maximized


```cpp
bool xk_window_is_maximized(const Ptr *win);
```
Gets weather the window is Maximized


```cpp
bool xk_window_is_visible(const Ptr *win);
```
Gets the window visibility


```cpp
bool xk_window_is_resizable(const Ptr *win);
```
Gets weather the window is resizable


```cpp
bool xk_window_is_decorated(const Ptr *win);
```
Gets weather the window is decorated


```cpp
void xk_window_set_fullscreen(const Ptr *win, bool fullscreen);
```
Sets weather the window is fullscreen


```cpp
void xk_window_set_decorations();
```
Sets the window decorations


```cpp
void xk_window_set_always_on_top(const Ptr *win, bool always_on_top);
```
Sets the window always on top


```cpp
void xk_window_request_user_attention(const Ptr *win, bool is_important, bool is_critical);
```
Request user attention to the window


```cpp
void xk_window_hide_menu(const Ptr *win);
```
Hides the window's menu


```cpp
void xk_window_show_menu(const Ptr *win);
```
Shows the window's menu



### EventLoop
```cpp
Ptr xk_eloop_new();
```
Creates a new Event Loop


```cpp
void xk_eloop_run(Ptr eloop, CEventData (*callback)(CEventData));
```
Runs the EventLoop, the event loop is free after the function has finsihed executed 

