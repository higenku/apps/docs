# Higenku Krome (XKrome) <span class="badge badge-warn">Work In progress</span>
The cross-platform webview library

## Reference
- [Architecture](./architecture)
- [Functions](./functions)
- [Examples](./examples)