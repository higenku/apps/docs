# Code Examples

## C++ / C
```cpp
#include "xkrome.h"
#include "iostream"
#include <cstring>

CEventData callback(CEventData data) {
      
    // if ( !std::strcmp(data._type, "WindowEvent") && !std::strcmp(data._subtype, "CloseRequested")) {
    //     std::cout << "Received a callback of type " << data._type << ":" << data._subtype << "\n";  
    //     std::cout << "Finishing call\n";
    //     return CEventData {
    //         ._type = "ControlFlow",
    //         ._subtype = "",
    //         ._data = "",
    //         ._result = "Exit",
    //     };
    // }
    return CEventData {
        ._type = "",
        ._subtype = "",
        ._data = "",
        ._result = "",
    };
}

int main() {
    std::cout << "Hello world\n";
    
    Ptr eloop = xk_eloop_new();
    std::cout << "1\n";
    Ptr window = xk_window_new(&eloop);
    Ptr webcontext = xk_webcontext_new("./xkrome_data");
    Ptr builder = xk_webview_builder_new();

    
    xk_webview_builder_with_url(&builder, "https://duck.com");

    std::cout << "2\n";
    xk_window_set_title(&window, "Hello world!");
    // xk_window_set_window_icon(&window, "/home/litch0/dev/Higenku/Sdks/XKrome/Source/examples/icon.png");
    xk_window_set_inner_size(&window, 1080, 900);
    std::cout << "3\n";
    
    xk_webview_builder_build(builder, window, &webcontext);
    xk_eloop_run(eloop, callback);
    std::cout << "4\n";
    std::cout << "Finished\n";
}
```

## C#

```csharp
// 

// program.cs
using System;
using System.Runtime.InteropServices;

namespace csexample
{

    [StructLayout(LayoutKind.Sequential)]
    struct CEventData {
        public string _type;
        public string _subtype;
        public string _data;
        public string _result;
    }

    class XKrome 
    {
        public const string LibName = "xkrome";
        public delegate CEventData callback(CEventData e);

        [DllImport(LibName)]
        public static extern IntPtr xk_eloop_new();
        [DllImport(LibName)]
        public static extern void xk_eloop_run(IntPtr eloopptr, callback callback);
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var eloop = XKrome.xk_eloop_new();
            XKrome.xk_eloop_run(eloop, (CEventData data ) => {
                System.Console.WriteLine($"Received A Event of type {data._type}::{data._subtype}");
                return new CEventData {
                    _data = "",
                    _subtype = "",
                    _type = "",
                    _result = "",
                };
            });
        }
    }
}
```