import { DocSearchModal, useDocSearchKeyboardEvents } from '@docsearch/react';
import {Algolia} from '~/config';
import { createPortal } from 'preact/compat';
import { useCallback, useEffect, useRef, useState } from 'preact/hooks';
interface Props {
	lang?: string;
	// labels: Pick<DocSearchTranslation, 'modal' | 'placeholder'>;
}

export default function Search({ lang = 'en', /*labels*/ }: Props) {
	const [isOpen, setIsOpen] = useState(false);
	const [initialQuery, setInitialQuery] = useState<string>();

	const onOpen = useCallback(() => {
		setIsOpen(true);
	}, [setIsOpen]);

	const onClose = useCallback(() => {
		setIsOpen(false);
	}, [setIsOpen]);

	const onInput = useCallback(
		(e) => {
			setIsOpen(true);
			setInitialQuery(e.key);
		},
		[setIsOpen, setInitialQuery]
	);

	useDocSearchKeyboardEvents({
		isOpen,
		onOpen,
		onClose,
		onInput,
	});

	return <>
        {isOpen && createPortal(
            <DocSearchModal
                initialQuery={initialQuery}
                initialScrollY={window.scrollY}
                onClose={onClose}
                indexName={Algolia.indexName}
                appId={Algolia.appId}
                apiKey={Algolia.apiKey}
                searchParameters={{ facetFilters: [[`lang:${lang}`]] }}
                getMissingResultsUrl={({ query }) => `https://gitlab.com/higenku/apps/docs/-/issues/new?issue[title]=[DOC MISSING]+Missing+results+for+query+%22${encodeURIComponent(query)}%22`}
                transformItems={(items) => {
                    return items.map((item) => {
                        const a = document.createElement('a');
                        a.href = item.url;
                        const hash = a.hash === '#overview' ? '' : a.hash;
                        return {
                            ...item,
                            url: `${a.pathname}${hash}`,
                        };
                    });
                }}
            />,
            document.body
        )}

            <button type="button" onClick={onOpen} className="search-input">
				<svg width="24" height="24" fill="none">
					<path
						d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
						stroke="currentColor"
						strokeWidth="2"
						strokeLinecap="round"
						strokeLinejoin="round"
					/>
				</svg>

				<span>Search</span>

				<span className="search-hint">
					<span className="sr-only">Press </span>
					<kbd>/</kbd>
					<span className="sr-only"> to search</span>
				</span>
			</button>

    </>
}