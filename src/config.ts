export const Site = {
    title: "XDocs",
    description: "Higenku Documentation",
    defaultLanguage: "en_US",
};

export const OpenGraph = {
    image: {
        src: "/favicon.png",
        alt: "Higenku Centrialized Documentation for all our software",
    },
    twitter: "higenku_project",
};

export const Langs = {
    English: "en",
    Deutsch: "de",
    Português: "pt",
};

// Uncomment this to add an "Edit this page" button to every page of documentation.
export const EditUrl = `https://gitlab.com/-/ide/project/higenku/apps/docs/edit/dev/-/`;

// Uncomment this to add an "Join our Community" button to every page of documentation.
export const CommunityInvite = `https://higenku.org/support`;
export interface File {
    path: string;
    pathName: string;
}
const Files = {}

for (const i in import.meta.glob('./pages/**/*')) {
    let file = i.slice("./pages".length, i.length) as string
    
    if (file.endsWith('index.md')) {
        Files[file.replace("/index.md", "")] = `src/pages${file}`
    }
        
    else if (file.endsWith('index.astro')) {
        let pathName = file.replace("/index.astro", "");
        if (pathName.length == 0) {
            pathName = "/"
        }

        Files[pathName] = `src/pages${file}`;
    }
    else 
        Files[file.replace(".md", "")] = `src/pages${file}`
}
export {Files}
// Uncomment this to enable site search.
// See "Algolia" section of the README for more information.
export const Algolia = {
    indexName: "XXXXXXXXXX",
    appId: "XXXXXXXXXX",
    apiKey: "XXXXXXXXXX",
};

export const Sidebar = {
    "/en/page-2": [
        { text: "Hello", header: true },
        { text: "Section Header", header: true },
        { text: "Introduction", link: "/en/" },
        { text: "Page 2", link: "/en/page-2" },
        { text: "Page 3", link: "/en/page-3" },

        { text: "Another Section", header: true },
        { text: "Page 4", link: "/en/page-4" },
    ],

    "/en": [
        { text: "", header: true },
        { text: "Section Header", header: true },
        { text: "Introduction", link: "/en/" },
        { text: "Page 2", link: "/en/page-2" },
        { text: "Page 3", link: "/en/page-3" },

        { text: "Another Section", header: true },
        { text: "Page 4", link: "/en/page-4" },
    ],
};

