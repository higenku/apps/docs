import { Langs, Sidebar } from './config';

export { Langs };
export const KNOWN_LANGUAGE_CODES = Object.values(Langs);
export const langPathRegex = /\/([a-z]{2}-?[A-Z]{0,2})\//;

export function getLanguageFromURL(pathname: string): string {
	const langCodeMatch = pathname.match(langPathRegex);
	return langCodeMatch ? langCodeMatch[1] : 'en';
}


export function getCurrentBar(pathname: string): string[] {
	let res = []
	for (let ele of Object.keys(Sidebar)) {

		if (pathname.startsWith(ele)) {
			res.push(ele)
		}

	}
	
	return res
}