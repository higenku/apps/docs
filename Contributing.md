# Higenku Documentation Supplementary Guide
Make sure you take a look at [this document](https://higenku.org/legal/contributing) before starting.

## Supplementary Guides
We have some additional guides for this project.

## How can I contribute?

There are many ways to contribute to the documentation. Review the following sections to find out which one is right for you.

### Reporting bugs and suggesting enhancements
Please use the issue to submit bugs and suggestions.

### Editing in GitLab
Use the link at the end of any page called `edit in gitlab` that will redirect you to GitLab's website with the page you were reading open for you to edit it. Then follow our [guide on MRs](https://gitlab.com/higenku/legal/-/blob/dev/Contributing.md#user-content-code-review-process).


## Writing style
We want this documentation to be accessible for all kinds of readers, independent of age and school level. For that to happen, we need to establish some basic rules when writing documentation. For us to achieve this, please prefer to use simple language that a 14-year-old would understand, add links to words that may be difficult to understand, add important details that make sense to the current context.

If you are unsure about a certain topic, just ask it in our [discord / matrix](https://higenku.org/contact).

## Structure
we follow a not so strict structure when possible that looks like this:<br>
`/{lang}/{group}/{program|library}/{page}`<br>
For example, xkrome is a webview library, so it will go in the SDKs group, the `{program|library}` will have the name of the program / library without the `x`. Webview library have functions, an architecture, and will need examples. So some pages will be `achiteture`, `functions` and `examples`. In the end, here are some examples of URLs: <br>
`/en/sdks/krome/functions` <br>
`/en/sdks/krome/architecture` <br>
`/en/sdks/krome/examples` <br>

## Project setup
we use [svelte kit](https://kit.svelte.dev/) and a custom documentation generator for this project, for you to get started is very simple, write the documentation you want in `src/docs`, then to test it out, use:
> at the current time, this project does not support watch files, so everytime you alter something in the docs, you need to rerun the command
```shell
pnpm dev 
```
for building the project into static files, use:
```shell
pnpm build
```

## End comments

Have fun :)